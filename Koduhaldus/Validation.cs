﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Koduhaldus
{
    public static class ValidationUtil
    {
        public static DateTime Limit(string propertyName)
        {
            switch (propertyName.ToLower())
            {
                case "täna":
                case "today": return DateTime.Today;
                case "eile":
                case "yesterday": return DateTime.Today.AddDays(-1);
                case "homme":
                case "tomorrow": return DateTime.Today.AddDays(+1);
                case "see aasta":
                case "this year": return new DateTime(DateTime.Now.Year, 1, 1);
            }
            if (DateTime.TryParse(propertyName, out DateTime d)) return d;
            return DateTime.MinValue; // see rida on vigane
        }
    }
    public sealed class MustBeGreaterThan : ValidationAttribute
    {
        private const string _defaultErrorMessage = "'{0}' - kuupäev peab olema >= {1}";
        private string _basePropertyName;
        private DateTime _validationLimit;

        public MustBeGreaterThan(string basePropertyName)
            : base(_defaultErrorMessage)
        {
            _basePropertyName = basePropertyName;
            _validationLimit = ValidationUtil.Limit(_basePropertyName);
        }

        //Override default FormatErrorMessage Method
        public override string FormatErrorMessage(string name)
        {
            return string.Format(_defaultErrorMessage, name, _basePropertyName);
        }

        //Override IsValid
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var basePropertyInfo = validationContext.ObjectType.GetProperty(_basePropertyName);
            var thisValue = (DateTime)value;

            if (thisValue < _validationLimit)
            {
                var message = FormatErrorMessage(validationContext.DisplayName);
                return new ValidationResult(message);
            }

            //value validated
            return null;
        }
    }

    public sealed class MustBeLowerThan : ValidationAttribute
    {
        private const string _defaultErrorMessage = "'{0}' - kuupäev peab olema <= {1}";
        private string _basePropertyName;
        private DateTime _validationLimit;

        public MustBeLowerThan(string basePropertyName)
            : base(_defaultErrorMessage)
        {
            _basePropertyName = basePropertyName;
            _validationLimit = ValidationUtil.Limit(_basePropertyName);
        }

        //Override default FormatErrorMessage Method
        public override string FormatErrorMessage(string name)
        {
            return string.Format(_defaultErrorMessage, name, _basePropertyName);
        }

        //Override IsValid
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var basePropertyInfo = validationContext.ObjectType.GetProperty(_basePropertyName);
            var thisValue = (DateTime)value;

            if (thisValue > _validationLimit)
            {
                var message = FormatErrorMessage(validationContext.DisplayName);
                return new ValidationResult(message);
            }

            //value validated
            return null;
        }
    }
}
