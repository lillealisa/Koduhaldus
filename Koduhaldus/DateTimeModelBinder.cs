﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using System.Web.Mvc;

namespace Koduhaldus
{
    public class DateTimeModelBinder : IModelBinder

    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (value == null)
                return null;
            if (string.IsNullOrEmpty(value.AttemptedValue))
                return null;

            try
            {
                var date = value.ConvertTo(typeof(DateTime), CultureInfo.GetCultureInfo("et-ee"));
                return date;
            }
            catch
            {
            
                var dateValue = value.AttemptedValue;
                var formats = new[] { "0:dd'/'MM'/'yyyy", "0:dd'/'MM'/'yyyy", "yyyy'/MM'/dd", "yyyy'/MM'/dd" };
                DateTime date;

                if (!DateTime.TryParseExact(dateValue, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    throw;
                }

                return date;
            }
        }


    }
}