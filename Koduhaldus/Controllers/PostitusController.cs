﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Koduhaldus;
using System.IO;
namespace Koduhaldus
{
    partial class Postitus
    {
        public string SisuLyhike => (Sisu ?? "").Length < 200 ? Sisu : Sisu.Substring(0, 200);
    }
}

namespace Koduhaldus.Controllers
{
    public class PostitusController : Controller
    {
        private KoduhaldusEntities db = new KoduhaldusEntities();

        // GET: Postitus
        public ActionResult Index()
        {
            var postitus = db.Postitus.Include(p => p.Kasutaja).Include(p => p.Kategooria);
            return View(postitus.ToList());
        }




        // GET: Postitus/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)

            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Postitus postitus = db.Postitus.Find(id);
            if (postitus == null)
            {
                return HttpNotFound();
            }

            Kasutaja u = db.Kasutaja.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
            if (u != null)
            {
                ViewBag.NewKommentaar = new Kommentaar { PostitusId = id.Value, KasutajaId = u.Id, Kuupäev = DateTime.Now, Sisu = "" };
                return View(postitus);
            }
            //else return RedirectToAction("Index");
            else
            {
                ViewBag.NewKommentaar = null;
                return View(postitus);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Details([Bind(Include = "Id,Pealkiri,Sisu,Kuupäev,KasutajaId,KategooriaId,PostitusId")] Kommentaar kommentaar)
        {


            if (ModelState.IsValid && kommentaar.Sisu != "")
            {
                db.Kommentaar.Add(kommentaar);
                db.SaveChanges();
                return RedirectToAction("Details");
            }

            return View(db.Postitus.Find(kommentaar.PostitusId));
        }


        // GET: Postitus/Create
        public ActionResult Create(int? id)
        {

            Kasutaja u = db.Kasutaja.Where(x => x.Email == User.Identity.Name).SingleOrDefault();

       
            if (u?.KasAdmin ?? false)
            {
                Postitus p = new Postitus();

                p.Kuupäev = DateTime.Now.Date;


                ViewBag.KasutajaNimi = db.Kasutaja.Where(x => x.Email == User.Identity.Name).SingleOrDefault()?.Nimi;
                ViewBag.KategooriaId = new SelectList(db.Kategooria, "Id", "Nimetus");
                ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Nimi", p.KasutajaId);
                return View(p);
            }
            else return RedirectToAction("Index");
        }

        // POST: Postitus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Pealkiri,Sisu,Kuupäev,KasutajaId,KategooriaId")] Postitus postitus)
        {
            if (ModelState.IsValid)
            {
                db.Postitus.Add(postitus);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Email", postitus.KasutajaId);
            ViewBag.KategooriaId = new SelectList(db.Kategooria, "Id", "Nimetus", postitus.KategooriaId);
            return View(postitus);
        }

        // GET: Postitus/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Postitus postitus = db.Postitus.Find(id);
            if (postitus == null)
            {
                return HttpNotFound();
            }
            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Email", postitus.KasutajaId);
            ViewBag.KategooriaId = new SelectList(db.Kategooria, "Id", "Nimetus", postitus.KategooriaId);
            return View(postitus);
        }

        // POST: Postitus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Pealkiri,Sisu,Kuupäev,KasutajaId,KategooriaId")] Postitus postitus)
        {
            if (ModelState.IsValid)
            {
                db.Entry(postitus).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Email", postitus.KasutajaId);
            ViewBag.KategooriaId = new SelectList(db.Kategooria, "Id", "Nimetus", postitus.KategooriaId);
            return View(postitus);
        }

        // GET: Postitus/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Postitus postitus = db.Postitus.Find(id);
            if (postitus == null)
            {
                return HttpNotFound();
            }
            return View(postitus);
        }

        // POST: Postitus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Postitus postitus = db.Postitus.Find(id);
            db.Postitus.Remove(postitus);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult CreatePicture(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
                try
                {
                    string path = Path.Combine(Server.MapPath("~/Images"),
                    Path.GetFileName(file.FileName));
                    file.SaveAs(path);
                    ViewBag.Message = "File uploaded successfully";
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            else
            {
                ViewBag.Message = "You have not specified a file.";
            }
            return RedirectToAction("Create");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
