﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Koduhaldus;

namespace Koduhaldus.Controllers
{
    public class KategooriasController : Controller
    {
        private KoduhaldusEntities db = new KoduhaldusEntities();//

        // GET: Kategoorias
        public ActionResult Index()
        {
            return View(db.Kategooria.ToList());
        }

        // GET: Kategoorias/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kategooria kategooria = db.Kategooria.Find(id);
            if (kategooria == null)
            {
                return HttpNotFound();
            }
            return View(kategooria);
        }

        // GET: Kategoorias/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Kategoorias/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nimetus")] Kategooria kategooria)
        {
            if (ModelState.IsValid)
            {
                db.Kategooria.Add(kategooria);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(kategooria);
        }

        // GET: Kategoorias/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kategooria kategooria = db.Kategooria.Find(id);
            if (kategooria == null)
            {
                return HttpNotFound();
            }
            return View(kategooria);
        }

        // POST: Kategoorias/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nimetus")] Kategooria kategooria)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kategooria).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kategooria);
        }

        // GET: Kategoorias/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kategooria kategooria = db.Kategooria.Find(id);
            if (kategooria == null)
            {
                return HttpNotFound();
            }
            return View(kategooria);
        }

        // POST: Kategoorias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kategooria kategooria = db.Kategooria.Find(id);
            db.Kategooria.Remove(kategooria);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
