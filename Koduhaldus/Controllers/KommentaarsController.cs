﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Koduhaldus;

namespace Koduhaldus.Controllers
{
    public class KommentaarsController : Controller
    {
        private KoduhaldusEntities db = new KoduhaldusEntities();

        // GET: Kommentaars
        public ActionResult Index()
        {
            var kommentaar = db.Kommentaar.Include(k => k.Kasutaja).Include(k => k.Postitus);
            return View(kommentaar.ToList());
        }

        // GET: Kommentaars/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kommentaar kommentaar = db.Kommentaar.Find(id);
            if (kommentaar == null)
            {
                return HttpNotFound();
            }
            return View(kommentaar);
        }
        //SingleOrDefault()?.Nimi
        // GET: Kommentaars/Create
        public ActionResult Create(int? id)
        {
            if (id == null) return HttpNotFound();

            Kasutaja u = db.Kasutaja.Where(x => x.Email == User.Identity.Name).SingleOrDefault();  
          
           
            Kommentaar k = new Kommentaar();
            k.PostitusId = id.Value;
            k.Kuupäev = DateTime.Now.Date;
            k.KasutajaId = u?.Id ?? 0; 
            ViewBag.PostitusId = new SelectList(db.Postitus, "Id", "Pealkiri",k.PostitusId);
            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Nimi", k.KasutajaId);
            return View(k);
            //return View(Postitus);
        }

        // POST: Kommentaars/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PostitusId,KasutajaId,Kuupäev,Sisu")] Kommentaar kommentaar)
        {
            if (ModelState.IsValid)
            {
                db.Kommentaar.Add(kommentaar);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Email", kommentaar.KasutajaId);
            ViewBag.PostitusId = new SelectList(db.Postitus, "Id", "Pealkiri", kommentaar.PostitusId);
            return View(kommentaar);
        }

        // GET: Kommentaars/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kommentaar kommentaar = db.Kommentaar.Find(id);
            if (kommentaar == null)
            {
                return HttpNotFound();
            }
            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Email", kommentaar.KasutajaId);
            ViewBag.PostitusId = new SelectList(db.Postitus, "Id", "Pealkiri", kommentaar.PostitusId);
            return View(kommentaar);
        }

        // POST: Kommentaars/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PostitusId,KasutajaId,Kuupäev,Sisu")] Kommentaar kommentaar)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kommentaar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Email", kommentaar.KasutajaId);
            ViewBag.PostitusId = new SelectList(db.Postitus, "Id", "Pealkiri", kommentaar.PostitusId);
            return View(kommentaar);
        }

        // GET: Kommentaars/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kommentaar kommentaar = db.Kommentaar.Find(id);
            if (kommentaar == null)
            {
                return HttpNotFound();
            }
            return View(kommentaar);
        }

        // POST: Kommentaars/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kommentaar kommentaar = db.Kommentaar.Find(id);
            db.Kommentaar.Remove(kommentaar);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
