﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Koduhaldus;
using Koduhaldus.Models;

namespace Koduhaldus.Controllers
{
    public class TeenusController : Controller
    {
        private KoduhaldusEntities db = new KoduhaldusEntities();

        // GET: Teenus
        public ActionResult Index()
        {
            ViewBag.Ostukorv = Ostukorv.Korv;
            return View(db.Teenus.ToList());
        }

        // GET: Teenus/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Teenus teenus = db.Teenus.Find(id);
            if (teenus == null)
            {
                return HttpNotFound();
            }
            return View(teenus);
        }

        // GET: Teenus/Create
        public ActionResult Create()

        {
            if (Request.IsAuthenticated)
            {
                
                return View();
            }
            else
            {
               return Redirect ("/");
            }

        }

        // POST: Teenus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,nimetus,liik,tunnihind,korrahind")] Teenus teenus)
        {
            if (ModelState.IsValid)
            {
                db.Teenus.Add(teenus);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(teenus);
        }

        public ActionResult Telli(int? id)
        {
            if (id.HasValue)
            {
                if (!Ostukorv.Korv.Keys.Contains(id.Value))
                    Ostukorv.Korv.Add(id.Value, new OstukorviRida { TeenusId = id ?? 0, Mitu = 0 });
                Ostukorv.Korv[id.Value].Mitu++;
            }
            return RedirectToAction("Index");
        }
        public ActionResult Maha(int? id)
        {
            if (id.HasValue)
            {
                if (Ostukorv.Korv.Keys.Contains(id.Value))
                {
                    Ostukorv.Korv[id.Value].Mitu--;
                    if (Ostukorv.Korv[id.Value].Mitu == 0) Ostukorv.Korv.Remove(id.Value);
                }
            }
            return RedirectToAction("Index");
        }


        // GET: Teenus/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Teenus teenus = db.Teenus.Find(id);
            if (teenus == null)
            {
                return HttpNotFound();
            }
            return View(teenus);
        }

        // POST: Teenus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,nimetus,liik,tunnihind,korrahind")] Teenus teenus)
        {
            if (ModelState.IsValid)
            {
                db.Entry(teenus).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(teenus);
        }

        // GET: Teenus/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Teenus teenus = db.Teenus.Find(id);
            if (teenus == null)
            {
                return HttpNotFound();
            }
            return View(teenus);
        }

        // POST: Teenus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Teenus teenus = db.Teenus.Find(id);
            db.Teenus.Remove(teenus);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
