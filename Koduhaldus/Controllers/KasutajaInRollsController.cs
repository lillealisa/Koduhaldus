﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Koduhaldus;

namespace Koduhaldus.Controllers
{
    public class KasutajaInRollsController : Controller
    {
        private KoduhaldusEntities db = new KoduhaldusEntities();

        // GET: KasutajaInRolls
        public ActionResult Index()
        {
            var kasutajaInRoll = db.KasutajaInRoll.Include(k => k.Kasutaja).Include(k => k.Roll);
            return View(kasutajaInRoll.ToList());
        }

        // GET: KasutajaInRolls/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KasutajaInRoll kasutajaInRoll = db.KasutajaInRoll.Find(id);
            if (kasutajaInRoll == null)
            {
                return HttpNotFound();
            }
            return View(kasutajaInRoll);
        }

        // GET: KasutajaInRolls/Create
        public ActionResult Create()
        {
            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Email");
            ViewBag.RolliId = new SelectList(db.Roll, "Id", "Nimetus");
            return View();
        }

        // POST: KasutajaInRolls/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,KasutajaId,RolliId")] KasutajaInRoll kasutajaInRoll)
        {
            if (ModelState.IsValid)
            {
                db.KasutajaInRoll.Add(kasutajaInRoll);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Email", kasutajaInRoll.KasutajaId);
            ViewBag.RolliId = new SelectList(db.Roll, "Id", "Nimetus", kasutajaInRoll.RolliId);
            return View(kasutajaInRoll);
        }

        // GET: KasutajaInRolls/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KasutajaInRoll kasutajaInRoll = db.KasutajaInRoll.Find(id);
            if (kasutajaInRoll == null)
            {
                return HttpNotFound();
            }
            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Email", kasutajaInRoll.KasutajaId);
            ViewBag.RolliId = new SelectList(db.Roll, "Id", "Nimetus", kasutajaInRoll.RolliId);
            return View(kasutajaInRoll);
        }

        // POST: KasutajaInRolls/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,KasutajaId,RolliId")] KasutajaInRoll kasutajaInRoll)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kasutajaInRoll).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Email", kasutajaInRoll.KasutajaId);
            ViewBag.RolliId = new SelectList(db.Roll, "Id", "Nimetus", kasutajaInRoll.RolliId);
            return View(kasutajaInRoll);
        }

        // GET: KasutajaInRolls/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KasutajaInRoll kasutajaInRoll = db.KasutajaInRoll.Find(id);
            if (kasutajaInRoll == null)
            {
                return HttpNotFound();
            }
            return View(kasutajaInRoll);
        }

        // POST: KasutajaInRolls/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            KasutajaInRoll kasutajaInRoll = db.KasutajaInRoll.Find(id);
            db.KasutajaInRoll.Remove(kasutajaInRoll);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
