﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Koduhaldus;
using Koduhaldus.Models;
using System.ComponentModel.DataAnnotations;

namespace Koduhaldus.Controllers
{
    public class TelliminesController : Controller
    {
        private KoduhaldusEntities db = new KoduhaldusEntities();

        // GET: Tellimines
        public ActionResult Index()
        {
            var tellimines = db.Tellimine.Include(t => t.Kasutaja).Include(t => t.Teenus);
            ViewBag.Ostukorv = Ostukorv.Korv;
            return View(tellimines.ToList());
        }

        // GET: Tellimines/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tellimine tellimine = db.Tellimine.Find(id);
            if (tellimine == null)
            {
                return HttpNotFound();
            }
            return View(tellimine);
        }

        // GET: Tellimines/Create
        public ActionResult Create()
        {
            ViewBag.Ostukorv = Ostukorv.Korv;
            ViewBag.KasutajaID = new SelectList(db.Kasutaja, "Id", "Email");
            //ViewBag.TeenusID = new SelectList(db.Teenus, "id", "nimetus");
            return View();
        }

        // POST: Tellimines/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Aadress,Kuupäev,Kellaaeg,KasutajaID,Telefon")] Tellimine tellimine)
        {
            if (ModelState.IsValid)
            {
                foreach (OstukorviRida or in Ostukorv.Korv.Values)
                {
                    db.Tellimine.Add(new Tellimine { Aadress = tellimine.Aadress, KasutajaID = tellimine.KasutajaID, Kellaaeg = tellimine.Kellaaeg, Kuupäev = tellimine.Kuupäev, Telefon = tellimine.Telefon, TeenusID = or.TeenusId });
                    db.SaveChanges();
                }
                return RedirectToAction("Blogi","Home");
            }
            //
            ViewBag.Ostukorv = Ostukorv.Korv;
            ViewBag.KasutajaID = new SelectList(db.Kasutaja, "Id", "Email", tellimine.KasutajaID);
            ViewBag.TeenusID = new SelectList(db.Teenus, "id", "nimetus", tellimine.TeenusID);
            return View(tellimine);
        }

        // GET: Tellimines/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tellimine tellimine = db.Tellimine.Find(id);
            if (tellimine == null)
            {
                return HttpNotFound();
            }
            ViewBag.KasutajaID = new SelectList(db.Kasutaja, "Id", "Email", tellimine.KasutajaID);
            ViewBag.TeenusID = new SelectList(db.Teenus, "id", "nimetus", tellimine.TeenusID);
            return View(tellimine);
        }

        // POST: Tellimines/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        

        public ActionResult Edit([Bind(Include = "Id,TeenusID,Aadress,Kuupäev,Kellaaeg,KasutajaID,Telefon")] Tellimine tellimine)
        {
            if (ModelState.IsValid)
            {
                
                db.Entry(tellimine).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KasutajaID = new SelectList(db.Kasutaja, "Id", "Email", tellimine.KasutajaID);
            ViewBag.TeenusID = new SelectList(db.Teenus, "id", "nimetus", tellimine.TeenusID);
            return View("Edit", tellimine);
        }

        // GET: Tellimines/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tellimine tellimine = db.Tellimine.Find(id);
            if (tellimine == null)
            {
                return HttpNotFound();
            }
            return View(tellimine);
        }

        // POST: Tellimines/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tellimine tellimine = db.Tellimine.Find(id);
            db.Tellimine.Remove(tellimine);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
