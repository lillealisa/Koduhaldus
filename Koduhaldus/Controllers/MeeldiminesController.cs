﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Koduhaldus;

namespace Koduhaldus.Controllers
{
    public class MeeldiminesController : Controller
    {
        private KoduhaldusEntities db = new KoduhaldusEntities();

        // GET: Meeldimines
        public ActionResult Index()
        {
            var meeldimines = db.Meeldimine.Include(m => m.Kasutaja).Include(m => m.Postitus);
            return View(meeldimines.ToList());
        }

        // GET: Meeldimines/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Meeldimine meeldimine = db.Meeldimine.Find(id);
            if (meeldimine == null)
            {
                return HttpNotFound();
            }
            return View(meeldimine);
        }

        // GET: Meeldimines/Create
        public ActionResult Create()
        {
            //ViewBag.KasutajaNimi = db.Kasutaja.Where(x => x.Email == User.Identity.Name).SingleOrDefault()?.Nimi;
            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Email");
            ViewBag.PostitusId = new SelectList(db.Postitus, "Id", "Pealkiri");
            return View();
        }

        // POST: Meeldimines/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PostitusId,KasutajaId,Hinne")] Meeldimine meeldimine)
        {
            if (ModelState.IsValid)
            {
                db.Meeldimine.Add(meeldimine);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Email", meeldimine.KasutajaId);
            ViewBag.PostitusId = new SelectList(db.Postitus, "Id", "Pealkiri", meeldimine.PostitusId);
            return View(meeldimine);
        }

        // GET: Meeldimines/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Meeldimine meeldimine = db.Meeldimine.Find(id);
            if (meeldimine == null)
            {
                return HttpNotFound();
            }
            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Email", meeldimine.KasutajaId);
            ViewBag.PostitusId = new SelectList(db.Postitus, "Id", "Pealkiri", meeldimine.PostitusId);
            return View(meeldimine);
        }

        // POST: Meeldimines/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PostitusId,KasutajaId,Hinne")] Meeldimine meeldimine)
        {
            if (ModelState.IsValid)
            {
                db.Entry(meeldimine).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Email", meeldimine.KasutajaId);
            ViewBag.PostitusId = new SelectList(db.Postitus, "Id", "Pealkiri", meeldimine.PostitusId);
            return View(meeldimine);
        }

        // GET: Meeldimines/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Meeldimine meeldimine = db.Meeldimine.Find(id);
            if (meeldimine == null)
            {
                return HttpNotFound();
            }
            return View(meeldimine);
        }

        // POST: Meeldimines/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Meeldimine meeldimine = db.Meeldimine.Find(id);
            db.Meeldimine.Remove(meeldimine);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
