﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Koduhaldus;



namespace Koduhaldus

{
    partial class Kasutaja
    {
        // selle rea võiks iga dataklassi laienduse ja kontrolleri algusse lisada
        static KoduhaldusEntities db = new KoduhaldusEntities();

        // teen ühe funktsiooni, mis annab meili järgi kasutaja
        public static Kasutaja OldByEmail(string email)
        {
            return db.Kasutaja.Where(x => x.Email == email).Take(1).SingleOrDefault();
        }

        public static Kasutaja ByEmail(String email)
        {
            Kasutaja kasutaja = OldByEmail(email);
            if (kasutaja == null)
            {
                db.Kasutaja.Add(kasutaja = new Kasutaja { Email = email, Nimi = "nimeta" });
                db.SaveChanges();
                db.KasutajaInRoll.Add(new KasutajaInRoll { KasutajaId = kasutaja.Id, RolliId = 0 });
                db.SaveChanges();

            }
            return kasutaja;
        }

        // lisan selle välja, et saaks oskuseid lisada
        public string UusOskus { get; set; }




    }
}


namespace Koduhaldus.Controllers
{
    public class KasutajasController : Controller
    {
        private KoduhaldusEntities db = new KoduhaldusEntities();

        // GET: Kasutajas
        public ActionResult Index()
        {
                
                Kasutaja kasutaja = db.Kasutaja.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
                if (kasutaja?.KasAdmin ?? false)
                {
                    // see kasutaja ON admin;
                    return View(db.Kasutaja.ToList());
                }

                else return RedirectToAction("Index", "Home");
            
        }

        // GET: Kasutajas/Details/5
        public ActionResult Details(int? id)
        {
           
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kasutaja kasutaja = db.Kasutaja.Find(id);
            if (kasutaja == null)
            {
                return HttpNotFound();
            }
            return View(kasutaja);
        }


        // GET: Kasutajas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Kasutajas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Email,KasAdmin,Nimi")] Kasutaja kasutaja)
        {
            if (ModelState.IsValid)
            {
                db.Kasutaja.Add(kasutaja);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(kasutaja);
        }

        // GET: Kasutajas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kasutaja kasutaja = db.Kasutaja.Find(id);
            if (kasutaja == null)
            {
                return HttpNotFound();
            }
            return View(kasutaja);
        }

        // POST: Kasutajas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Email,KasAdmin,Nimi")] Kasutaja kasutaja)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kasutaja).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kasutaja);
        }

        // GET: Kasutajas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kasutaja kasutaja = db.Kasutaja.Find(id);
            if (kasutaja == null)
            {
                return HttpNotFound();
            }
            return View(kasutaja);
        }

        // POST: Kasutajas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kasutaja kasutaja = db.Kasutaja.Find(id);
            db.Kasutaja.Remove(kasutaja);
            db.SaveChanges();
            return RedirectToAction("Index");
        }




        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
    
    
}

