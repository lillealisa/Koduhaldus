﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Koduhaldus.Models
{
    public class OstukorviRida
    {
        KoduhaldusEntities db = new KoduhaldusEntities();
        public int TeenusId { get; set; }
        public int Mitu { get; set; }
        public Teenus Teenus => db.Teenus.Find(TeenusId);
    }

    public class Ostukorv
    {
        //public static Dictionary<int, OstukorviRida> Korv = new Dictionary<int, OstukorviRida>();
        public static Dictionary<int, OstukorviRida> Korv
        {
            get

            {
                Dictionary<int, OstukorviRida> korv = System.Web.HttpContext.Current.Session["ostukorv"] as Dictionary<int, OstukorviRida>; 
                if (korv == null)
                {
                    korv = new Dictionary<int, OstukorviRida>();
                    System.Web.HttpContext.Current.Session["ostukorv"] = korv;
                }
                return korv; 
            }
        }
       
        public static implicit operator string(Ostukorv v)
        {
            throw new NotImplementedException();
        }
    }
}
