﻿SET IDENTITY_INSERT [dbo].[Teenus] ON
INSERT INTO [dbo].[Teenus] ([id], [nimetus], [liik], [tunnihind], [korrahind]) VALUES (2, N'Triikimine', N'Lisateenus', CAST(0.00 AS Decimal(5, 2)), CAST(25.00 AS Decimal(5, 2)))
INSERT INTO [dbo].[Teenus] ([id], [nimetus], [liik], [tunnihind], [korrahind]) VALUES (3, N'Ahjukütmine', N'Lisateenus', NULL, CAST(25.00 AS Decimal(5, 2)))
INSERT INTO [dbo].[Teenus] ([id], [nimetus], [liik], [tunnihind], [korrahind]) VALUES (4, N'Lillede kastmine', N'Lisateenus', NULL, CAST(25.00 AS Decimal(5, 2)))
INSERT INTO [dbo].[Teenus] ([id], [nimetus], [liik], [tunnihind], [korrahind]) VALUES (5, N'Lemmikloomahoid', N'Põhiteenus', CAST(20.00 AS Decimal(5, 2)), NULL)
INSERT INTO [dbo].[Teenus] ([id], [nimetus], [liik], [tunnihind], [korrahind]) VALUES (6, N'Söögivalmistamine', N'Põhiteenus', CAST(20.00 AS Decimal(5, 2)), NULL)
INSERT INTO [dbo].[Teenus] ([id], [nimetus], [liik], [tunnihind], [korrahind]) VALUES (7, N'Kodukoristus', N'Põhiteenus', CAST(20.00 AS Decimal(5, 2)), NULL)
SET IDENTITY_INSERT [dbo].[Teenus] OFF
